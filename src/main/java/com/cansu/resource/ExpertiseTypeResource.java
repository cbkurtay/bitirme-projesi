package com.cansu.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cansu.dto.ExpertiseTypeDTO;
import com.cansu.service.ExpertiseTypeService;

@Component
@Path("/expertiseType")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ExpertiseTypeResource {

    @Autowired
    ExpertiseTypeService expertiseTypeService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<ExpertiseTypeDTO> expertiseTypeDTOS;
        try {
            expertiseTypeDTOS = expertiseTypeService.findAll();

            if (expertiseTypeDTOS.isEmpty()) {
                Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception ex) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertiseTypeDTOS).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        ExpertiseTypeDTO expertiseTypeDTO;
        try {
            expertiseTypeDTO = expertiseTypeService.findExpertiseTypeById(id);

            if (expertiseTypeDTO == null) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertiseTypeDTO).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        try {
            expertiseTypeService.delete(expertiseTypeService.findExpertiseTypeById(id));
        } catch (Exception ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(ExpertiseTypeDTO expertiseTypeDTO) {
        try {
            expertiseTypeDTO = expertiseTypeService.save(expertiseTypeDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertiseTypeDTO).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(ExpertiseTypeDTO expertiseTypeDTO, @PathParam("id") Long id) {
        try {
            expertiseTypeDTO = expertiseTypeService.update(expertiseTypeDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertiseTypeDTO).build();
    }

}
