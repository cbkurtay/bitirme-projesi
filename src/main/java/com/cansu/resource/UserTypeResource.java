package com.cansu.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cansu.dto.UserTypeDTO;
import com.cansu.service.UserTypeService;

@Component
@Path("/userType")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserTypeResource {

    @Autowired
    UserTypeService userTypeService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<UserTypeDTO> userTypeDTOS;
        try {
            userTypeDTOS = userTypeService.findAll();

            if (userTypeDTOS.isEmpty()) {
                Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(userTypeDTOS).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        UserTypeDTO userTypeDTO;
        try {
            userTypeDTO = userTypeService.findUserTypeFindById(id);

            if (userTypeDTO == null) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception ex) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(userTypeDTO).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        try {
            userTypeService.delete(userTypeService.findUserTypeFindById(id));
        } catch (Exception ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(UserTypeDTO userTypeDTO) {
        try {
            userTypeDTO = userTypeService.save(userTypeDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(userTypeDTO).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(UserTypeDTO userTypeDTO, @PathParam("id") Long id) {
        try {
            userTypeDTO = userTypeService.update(userTypeDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(userTypeDTO).build();
    }
}
