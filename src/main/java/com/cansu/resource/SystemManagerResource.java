package com.cansu.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cansu.dto.SystemManagerDTO;
import com.cansu.service.SystemManagerService;

@Component
@Path("/systemManager")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SystemManagerResource {

    @Autowired
    SystemManagerService sytemManagerService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<SystemManagerDTO> systemManagerDTOS;
        try {
            systemManagerDTOS = sytemManagerService.findAll();

            if (systemManagerDTOS.isEmpty()) {
                Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(systemManagerDTOS).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {

        SystemManagerDTO systemManagerDTO;
        try {
            systemManagerDTO = sytemManagerService.findSystemManagerFindById(id);

            if (systemManagerDTO == null) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(systemManagerDTO).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        try {
            sytemManagerService.delete(sytemManagerService.findSystemManagerFindById(id));
        } catch (Exception ex) {

            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(SystemManagerDTO systemManagerDTO) {
        try {
            systemManagerDTO = sytemManagerService.save(systemManagerDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(systemManagerDTO).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(SystemManagerDTO systemManagerDTO, @PathParam("id") Long id) {
        try {
            systemManagerDTO = sytemManagerService.update(systemManagerDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(systemManagerDTO).build();
    }

}
