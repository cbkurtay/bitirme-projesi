package com.cansu.resource;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cansu.service.ExpertService;
import com.cansu.dto.ExpertDTO;

@Component
@Path("/expert")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ExpertResource {

    @Autowired
    private ExpertService expertService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<ExpertDTO> expertDTOs;
        try {
            expertDTOs = expertService.findAll();

            if (expertDTOs.isEmpty()) {
                Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception ex) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertDTOs).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        ExpertDTO expertDTO;
        try {
            expertDTO = expertService.findExpertById(id);

            if (expertDTO == null) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception ex) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertDTO).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        try {
            expertService.delete(expertService.findExpertById(id));
        } catch (Exception ex) {

            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(ExpertDTO expertDTO) {
        try {
            expertDTO = expertService.save(expertDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertDTO).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(ExpertDTO expertDTO, @PathParam("id") Long id) {
        try {
            expertDTO = expertService.update(expertDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(expertDTO).build();
    }

}
