package com.cansu.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cansu.dto.ProjectDTO;
import com.cansu.service.ProjectServiceImp;

@Component
@Path("/project")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectResource {

    @Autowired
    ProjectServiceImp projectService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<ProjectDTO> projectDTOs;
        try {
            projectDTOs = projectService.findAll();

            if (projectDTOs.isEmpty()) {
                Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception ex) {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(projectDTOs).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {

        ProjectDTO projectDTO;
        try {
            projectDTO = projectService.findProjectFindById(id);

            if (projectDTO == null) {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(projectDTO).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        try {
            projectService.delete(projectService.findProjectFindById(id));
        } catch (Exception ex) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(ProjectDTO projectDTO) {
        try {
            projectDTO = projectService.save(projectDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(projectDTO).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(ProjectDTO projectDTO, @PathParam("id") Long id) {
        try {
            projectDTO = projectService.update(projectDTO);
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(projectDTO).build();
    }

}
