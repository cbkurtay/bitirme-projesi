package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.UserTypeDTO;


@Entity
@Table(name = "USERTYPE")
@NamedQueries({ @NamedQuery(name = "findAllUserType", query = "Select ut from UserType ut"),
        @NamedQuery(name = "findUserTypeById", query = "Select ut from UserType ut where ut.id=:id") })
public class UserType {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserTypeDTO toDTO() {
        UserTypeDTO userTypeDTO = new UserTypeDTO();
        userTypeDTO.setId(this.id);
        userTypeDTO.setName(this.name);

        return userTypeDTO;
    }
}
