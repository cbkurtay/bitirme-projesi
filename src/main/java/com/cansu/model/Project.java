package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.ProjectDTO;

@Entity
@Table(name = "PROJECT")
@NamedQueries({ @NamedQuery(name = "findAllProject", query = "Select p from Project p"),
        @NamedQuery(name = "findProjectById", query = "Select p from Project p where p.id=:id") })
public class Project {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SUMMARY")
    private String summary;

    @Column(name = "KEYWORDS")
    private String keywords;

    @Column(name = "DOCUMENT")
    private String document;

    @Column(name = "EXPERTEVALUATION")
    private String expertEvaluation;

    @ManyToOne
    @JoinColumn(name = "MEMBERID", referencedColumnName = "ID")
    private Member member;

    @ManyToOne
    @JoinColumn(name = "EXPERTID", referencedColumnName = "ID")
    private Expert expert;

    @ManyToOne
    @JoinColumn(name = "REFEREEID", referencedColumnName = "ID")
    private Project referee;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        this.expert = expert;
    }

    public Project getReferee() {
        return referee;
    }

    public void setReferee(Project referee) {
        this.referee = referee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getExpertEvaluation() {
        return expertEvaluation;
    }

    public void setExpertEvaluation(String expertEvaluation) {
        this.expertEvaluation = expertEvaluation;
    }

    public ProjectDTO toDTO() {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(this.id);
        projectDTO.setDocument(this.document);
        projectDTO.setExpertEvaluation(this.expertEvaluation);
        projectDTO.setKeywords(this.keywords);
        projectDTO.setName(this.name);
        projectDTO.setSummary(this.summary);
        projectDTO.setTitle(this.title);

        return projectDTO;
    }
}
