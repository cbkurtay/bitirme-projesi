package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.RefereeDTO;

@Entity
@Table(name = "REFEREE")
@NamedQueries({ @NamedQuery(name = "findAllReferee", query = "Select r from Referee r"),
        @NamedQuery(name = "findRefereeId", query = "Select r from Referee r where r.id=:id") })
public class Referee {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToOne
    @JoinColumn(name = "USERTYPEID", referencedColumnName = "ID")
    private UserType userType;

    @ManyToOne
    @JoinColumn(name = "EXPERTISETYPE", referencedColumnName = "ID")
    private UserType expertiseType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public UserType getExpertiseType() {
        return expertiseType;
    }

    public void setExpertiseType(UserType expertiseType) {
        this.expertiseType = expertiseType;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RefereeDTO toDTO() {
        RefereeDTO refereeDTO = new RefereeDTO();
        refereeDTO.setId(this.id);
        refereeDTO.setMail(this.mail);
        refereeDTO.setName(this.name);
        refereeDTO.setPassword(this.password);
        refereeDTO.setSurname(this.surname);
        refereeDTO.setUserName(this.userName);

        return refereeDTO;
    }
}
