package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.ExpertiseTypeDTO;

@Entity
@Table(name = "EXPERTISETYPE")
@NamedQueries({
        @NamedQuery(name = "findAllExpertiseType", query = "Select et from ExpertiseType et"),
        @NamedQuery(name = "findExpertiseTypeById", query = "Select et from ExpertiseType et where et.id=:id") })
public class ExpertiseType {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExpertiseTypeDTO toDTO() {

        ExpertiseTypeDTO expertiseTypeDto = new ExpertiseTypeDTO();
        expertiseTypeDto.setId(this.id);
        expertiseTypeDto.setName(this.name);

        return expertiseTypeDto;
    }
}
