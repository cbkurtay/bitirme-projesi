package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.ManagerDTO;

@Entity
@Table(name = "MANAGER")
@NamedQueries({ @NamedQuery(name = "findAllManager", query = "Select m from Manager m"),
        @NamedQuery(name = "findManagerById", query = "Select m from Manager m where m.id=:id") })
public class Manager {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "MAIL")
    private String mail;

    @ManyToOne
    @JoinColumn(name = "USERTYPEID", referencedColumnName = "ID")
    private UserType userType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public ManagerDTO toDTO() {
        ManagerDTO managerDTO = new ManagerDTO();
        managerDTO.setId(this.id);
        managerDTO.setMail(this.mail);
        managerDTO.setName(this.name);
        managerDTO.setPassword(this.password);
        managerDTO.setSurname(this.surname);
        managerDTO.setUserName(this.userName);

        return managerDTO;

    }
}
