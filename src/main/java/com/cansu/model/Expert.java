package com.cansu.model;
import javax.persistence.*;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;
import com.cansu.dto.ExpertDTO;


@Entity
@Table(name = "EXPERT")
@NamedQueries({ @NamedQuery(name = "findAllExpert", query = "Select e from Expert e"),
        @NamedQuery(name = "findExpertById", query = "Select e from Expert e where e.id=:id") })
public class Expert {

    @Id
    @Column (name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToOne
    @JoinColumn(name = "USERTYPEID", referencedColumnName = "ID")
    private UserType userType;

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ExpertDTO toDTO() {
        ExpertDTO expertDTO = new ExpertDTO();
        expertDTO.setId(this.id);
        expertDTO.setMail(this.mail);
        expertDTO.setName(this.name);
        expertDTO.setPassword(this.password);
        expertDTO.setSurname(this.surname);
        expertDTO.setUserName(this.userName);

        return expertDTO;
    }

}
