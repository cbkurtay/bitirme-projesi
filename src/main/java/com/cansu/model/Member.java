package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.MemberDTO;

@Entity
@Table(name = "MEMBER")
@NamedQueries({ @NamedQuery(name = "findAllMember", query = "Select m from Member m"),
        @NamedQuery(name = "findMemberById", query = "Select m from Member m where m.id=:id") })
public class Member {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "EXPERIENCE")
    private String experience;

    @Column(name = "LICENCESTATUS")
    private String licenceStatus;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToOne
    @JoinColumn(name = "USERTYPEID", referencedColumnName = "ID")
    private UserType userType;

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLicenceStatus() {
        return licenceStatus;
    }

    public void setLicenceStatus(String licenceStatus) {
        this.licenceStatus = licenceStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MemberDTO toDTO() {
        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setId(this.id);
        memberDTO.setExperience(this.experience);
        memberDTO.setLicenceStatus(this.licenceStatus);
        memberDTO.setMail(this.mail);
        memberDTO.setName(this.name);
        memberDTO.setPassword(this.password);
        memberDTO.setSurname(this.surname);
        memberDTO.setUserName(this.userName);

        return memberDTO;

    }
}
