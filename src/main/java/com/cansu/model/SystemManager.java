package com.cansu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.NamedQueries;

import com.cansu.dto.SystemManagerDTO;

@Entity
@Table(name = "SYSTEMMANAGER")
@NamedQueries({
        @NamedQuery(name = "findAllSystemManager", query = "Select s from SystemManager  s"),
        @NamedQuery(name = "findSystemManagerById", query = "Select s from SystemManager s where s.id=:id") })
public class SystemManager {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "MAIL")
    private String mail;

    @ManyToOne
    @JoinColumn(name = "USERTYPEID", referencedColumnName = "ID")
    private UserType userType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public SystemManagerDTO toDTO() {
        SystemManagerDTO systemManagerDTO = new SystemManagerDTO();
        systemManagerDTO.setId(this.id);
        systemManagerDTO.setMail(this.mail);
        systemManagerDTO.setName(this.name);
        systemManagerDTO.setPassword(this.password);
        systemManagerDTO.setSurname(this.surname);
        systemManagerDTO.setUserName(this.userName);

        return systemManagerDTO;
    }
}
