package com.cansu.service;

import com.cansu.dto.ProjectDTO;

import java.util.List;

/**
 * Created by cansubahar on 15.04.2017.
 */
public interface ProjectService {

    List<ProjectDTO> findAll();

    ProjectDTO save(ProjectDTO projectDTO);

    ProjectDTO update(ProjectDTO projectDTO);

    ProjectDTO delete(ProjectDTO projectDTO);

    public ProjectDTO findProjectFindById(Long id);


}
