package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cansu.dao.RefereeDao;
import com.cansu.model.Referee;
import com.cansu.dto.RefereeDTO;

@Service
public class RefereeService {

    @Autowired
    RefereeDao refereeDao;

    @Transactional(readOnly = true)
    public List<RefereeDTO> findAll() {
        List<RefereeDTO> refereeDTOs = new ArrayList<RefereeDTO>();

        for (Referee referee : refereeDao.findAll()) {
            refereeDTOs.add(referee.toDTO());
        }
        return refereeDTOs;
    }

    @Transactional
    public RefereeDTO save(RefereeDTO refereeDTO) {
        return refereeDao.save(refereeDTO.toEntity()).toDTO();
    }

    @Transactional
    public RefereeDTO update(RefereeDTO refereeDTO) {
        return refereeDao.update(refereeDTO.toEntity()).toDTO();
    }

    @Transactional
    public RefereeDTO delete(RefereeDTO refereeDTO) {
        return refereeDao.delete(refereeDTO.toEntity()).toDTO();
    }

    @Transactional
    public RefereeDTO findRefereeFindById(Long id) {
        return refereeDao.findById(id).toDTO();
    }

}
