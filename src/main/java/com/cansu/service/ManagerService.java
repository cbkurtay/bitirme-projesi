package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cansu.dao.ManagerDao;
import com.cansu.model.Manager;
import com.cansu.dto.ManagerDTO;

@Service
public class ManagerService {

    @Autowired
    ManagerDao managerDao;

    @Transactional(readOnly = true)
    public List<ManagerDTO> findAll() {

        List<ManagerDTO> managerDTOs = new ArrayList<ManagerDTO>();

        for (Manager manager : managerDao.findAll()) {
            managerDTOs.add(manager.toDTO());
        }
        return managerDTOs;
    }

    @Transactional
    public ManagerDTO save(ManagerDTO managerDTO) {
        return managerDao.save(managerDTO.toEntity()).toDTO();
    }

    @Transactional
    public ManagerDTO update(ManagerDTO managerDTO) {
        return managerDao.update(managerDTO.toEntity()).toDTO();
    }

    @Transactional
    public ManagerDTO delete(ManagerDTO managerDTO) {
        return managerDao.delete(managerDTO.toEntity()).toDTO();
    }

    @Transactional
    public ManagerDTO findManagerById(Long id) {
        return managerDao.findById(id).toDTO();
    }

}
