package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cansu.dao.SystemManagerDao;
import com.cansu.model.SystemManager;
import com.cansu.dto.SystemManagerDTO;

@Service
public class SystemManagerService {

    @Autowired
    SystemManagerDao sytemManagerDao;

    public List<SystemManagerDTO> findAll() {
        List<SystemManagerDTO> systemManagerDTOs = new ArrayList<SystemManagerDTO>();

        for (SystemManager systemManager : sytemManagerDao.findAll()) {
            systemManagerDTOs.add(systemManager.toDTO());
        }
        return systemManagerDTOs;
    }

    public SystemManagerDTO save(SystemManagerDTO systemManagerDTO) {
        return sytemManagerDao.save(systemManagerDTO.toEntity()).toDTO();
    }

    public SystemManagerDTO update(SystemManagerDTO systemManagerDTO) {
        return sytemManagerDao.update(systemManagerDTO.toEntity()).toDTO();
    }

    public SystemManagerDTO delete(SystemManagerDTO systemManagerDTO) {
        return sytemManagerDao.delete(systemManagerDTO.toEntity()).toDTO();
    }

    public SystemManagerDTO findSystemManagerFindById(Long id) {
        return sytemManagerDao.findById(id).toDTO();
    }

}
