package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cansu.dao.MemberDao;
import com.cansu.model.Member;
import com.cansu.dto.MemberDTO;

@Service
public class MemberService {

    @Autowired
    MemberDao memberDao;

    public List<MemberDTO> findAll() {
        List<MemberDTO> memberDTOs = new ArrayList<MemberDTO>();

        for (Member member : memberDao.findAll()) {
            memberDTOs.add(member.toDTO());
        }
        return memberDTOs;
    }

    public MemberDTO save(MemberDTO memberDTO) {
        return memberDao.save(memberDTO.toEntity()).toDTO();
    }

    public MemberDTO update(MemberDTO memberDTO) {
        return memberDao.update(memberDTO.toEntity()).toDTO();
    }

    public MemberDTO delete(MemberDTO memberDTO) {
        return memberDao.delete(memberDTO.toEntity()).toDTO();
    }

    public MemberDTO findMemberById(Long id) {
        return memberDao.findById(id).toDTO();
    }

}
