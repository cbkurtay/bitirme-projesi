package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cansu.dao.ProjectDao;
import com.cansu.model.Project;
import com.cansu.dto.ProjectDTO;

@Service
public class ProjectServiceImp implements ProjectService {

    @Autowired
    ProjectDao projectDao;

    @Transactional(readOnly = true)
    public List<ProjectDTO> findAll() {
        List<ProjectDTO> projectDTOs = new ArrayList<ProjectDTO>();

        for (Project project : projectDao.findAll()) {
            projectDTOs.add(project.toDTO());
        }
        return projectDTOs;
    }

    @Transactional
    public ProjectDTO save(ProjectDTO projectDTO) {
        return projectDao.save(projectDTO.toEntity()).toDTO();
    }

    @Transactional
    public ProjectDTO update(ProjectDTO projectDTO) {
        return projectDao.update(projectDTO.toEntity()).toDTO();
    }

    @Transactional
    public ProjectDTO delete(ProjectDTO projectDTO) {
        return projectDao.delete(projectDTO.toEntity()).toDTO();
    }

    @Transactional
    public ProjectDTO findProjectFindById(Long id) {
        return projectDao.findById(id).toDTO();
    }

}
