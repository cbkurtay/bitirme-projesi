package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cansu.dao.ExpertiseTypeDao;
import com.cansu.model.ExpertiseType;
import com.cansu.dto.ExpertiseTypeDTO;

@Service
public class ExpertiseTypeService {

    @Autowired
    ExpertiseTypeDao expertiseTypeDao;

    @Transactional(readOnly = true)
    public List<ExpertiseTypeDTO> findAll() {

        List<ExpertiseTypeDTO> expertiseTypeDTOs = new ArrayList<ExpertiseTypeDTO>();

        for (ExpertiseType expertiseType : expertiseTypeDao.findAll()) {
            expertiseTypeDTOs.add(expertiseType.toDTO());
        }
        return expertiseTypeDTOs;
    }

    @Transactional
    public ExpertiseTypeDTO save(ExpertiseTypeDTO expertiseTypeDTO) {
        return expertiseTypeDao.save(expertiseTypeDTO.toEntity()).toDTO();
    }

    @Transactional
    public ExpertiseTypeDTO delete(ExpertiseTypeDTO expertiseTypeDTO) {
        return expertiseTypeDao.delete(expertiseTypeDTO.toEntity()).toDTO();
    }

    @Transactional
    public ExpertiseTypeDTO update(ExpertiseTypeDTO expertiseTypeDTO) {
        return expertiseTypeDao.update(expertiseTypeDTO.toEntity()).toDTO();
    }

    @Transactional
    public ExpertiseTypeDTO findExpertiseTypeById(Long id) {
        return expertiseTypeDao.findById(id).toDTO();
    }

}
