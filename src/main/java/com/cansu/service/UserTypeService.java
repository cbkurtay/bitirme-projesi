package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cansu.dao.UserTypeDao;
import com.cansu.model.UserType;
import com.cansu.dto.UserTypeDTO;

@Service
public class UserTypeService {

    @Autowired
    UserTypeDao userTypeDao;

    public List<UserTypeDTO> findAll() {
        List<UserTypeDTO> userTypeDTOs = new ArrayList<UserTypeDTO>();

        for (UserType userType : userTypeDao.findAll()) {
            userTypeDTOs.add(userType.toDTO());
        }
        return userTypeDTOs;
    }

    public UserTypeDTO save(UserTypeDTO userTypeDTO) {
        return userTypeDao.save(userTypeDTO.toEntity()).toDTO();
    }

    public UserTypeDTO update(UserTypeDTO userTypeDTO) {
        return userTypeDao.update(userTypeDTO.toEntity()).toDTO();
    }

    public UserTypeDTO delete(UserTypeDTO userTypeDTO) {
        return userTypeDao.delete(userTypeDTO.toEntity()).toDTO();
    }

    public UserTypeDTO findUserTypeFindById(Long id) {
        return userTypeDao.findById(id).toDTO();
    }

}
