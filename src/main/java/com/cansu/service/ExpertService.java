package com.cansu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cansu.dto.ExpertDTO;
import com.cansu.dao.ExpertDao;
import com.cansu.model.Expert;

@Service
public class ExpertService {

    @Autowired
    ExpertDao expertDao;

    @Transactional(readOnly = true)
    public List<ExpertDTO> findAll() {

        List<ExpertDTO> expertDTOs = new ArrayList<ExpertDTO>();

        for (Expert expert : expertDao.findAll()) {
            expertDTOs.add(expert.toDTO());
        }
        return expertDTOs;
    }

    @Transactional
    public ExpertDTO save(ExpertDTO expertDTO) {
        return expertDao.save(expertDTO.toEntity()).toDTO();
    }

    @Transactional
    public ExpertDTO delete(ExpertDTO expertDTO) {
        return expertDao.delete(expertDTO.toEntity()).toDTO();
    }

    @Transactional
    public ExpertDTO findExpertById(Long id) {
        return expertDao.findById(id).toDTO();
    }

    @Transactional
    public ExpertDTO update(ExpertDTO expertDTO) {
        return expertDao.update(expertDTO.toEntity()).toDTO();
    }

}
