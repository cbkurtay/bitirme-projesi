package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;


import com.cansu.model.Referee;

@Component
public class RefereeDao {

    @PersistenceContext
    EntityManager em;

    public List<Referee> findAll() {
        List<Referee> list = em.createNamedQuery("findAllReferee").getResultList();
        return list;
    }

    public Referee save(Referee referee) {
        em.persist(referee);
        return referee;
    }

    public Referee update(Referee referee) {
        em.merge(referee);
        return referee;
    }

    public Referee delete(Referee referee) {
        em.remove(referee);
        return referee;
    }

    public Referee findById(Long id) {
        Referee referee = em.createNamedQuery("findRefereeId", Referee.class).setParameter("id", id).getSingleResult();
        return referee;
    }

}
