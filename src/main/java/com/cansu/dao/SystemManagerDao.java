package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.SystemManager;

@Component
public class SystemManagerDao {

    @PersistenceContext
    EntityManager em;

    public List<SystemManager> findAll() {
        List<SystemManager> list = em.createNamedQuery("findAllSystemManager").getResultList();
        return list;
    }

    public SystemManager save(SystemManager systemManager) {
        em.persist(systemManager);
        return systemManager;
    }

    public SystemManager update(SystemManager sytemManager) {
        em.merge(sytemManager);
        return sytemManager;
    }

    public SystemManager delete(SystemManager systemManager) {
        em.remove(systemManager);
        return systemManager;
    }

    public SystemManager findById(Long id) {
        SystemManager systemManager = em.createNamedQuery("findSystemManagerById", SystemManager.class)
                .setParameter("id", id).getSingleResult();
        return systemManager;
    }

}
