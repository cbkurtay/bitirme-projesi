package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;


import com.cansu.model.ExpertiseType;

@Component
public class ExpertiseTypeDao {

    @PersistenceContext
    EntityManager em;

    public List<ExpertiseType> findAll() {
        List<ExpertiseType> list = em.createNamedQuery("findAllExpertiseType").getResultList();
        return list;
    }

    public ExpertiseType save(ExpertiseType expertiseType) {
        em.persist(expertiseType);
        return expertiseType;
    }

    public ExpertiseType delete(ExpertiseType expertiseType) {
        em.remove(expertiseType.getId());
        return expertiseType;
    }

    public ExpertiseType update(ExpertiseType expertiseType) {
        em.merge(expertiseType);
        return expertiseType;
    }

    public ExpertiseType findById(Long id) {
        ExpertiseType expertiseType = em.createNamedQuery("findExpertiseTypeById", ExpertiseType.class)
                .setParameter("id", id).getSingleResult();
        return expertiseType;
    }

}
