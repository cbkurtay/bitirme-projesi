package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.Manager;

@Component
public class ManagerDao {

    @PersistenceContext
    EntityManager em;

    public List<Manager> findAll() {
        List<Manager> list = em.createNamedQuery("findAllManager").getResultList();
        return list;
    }

    public Manager save(Manager manager) {
        em.persist(manager);
        return manager;
    }

    public Manager update(Manager manager) {
        em.merge(manager);
        return manager;
    }

    public Manager delete(Manager manager) {
        em.remove(manager.getId());
        return manager;
    }

    public Manager findById(Long id) {
        Manager manager = em.createNamedQuery("findManagerById", Manager.class).setParameter("id", id)
                .getSingleResult();
        return manager;
    }

}
