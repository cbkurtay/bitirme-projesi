package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.Project;

@Component
public class ProjectDao {

    @PersistenceContext
    EntityManager em;

    public List<Project> findAll() {
        List<Project> list = em.createNamedQuery("findAllProject").getResultList();
        return list;
    }

    public Project save(Project project) {
        em.persist(project);
        return project;
    }

    public Project delete(Project project) {
        em.remove(project);
        return project;
    }

    public Project update(Project project) {
        em.merge(project);
        return project;
    }

    public Project findById(Long id) {
        Project project = em.createNamedQuery("findProjectById", Project.class).setParameter("id", id)
                .getSingleResult();
        return project;
    }

}
