package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.Expert;

@Component
public class ExpertDao {

    @PersistenceContext
    EntityManager em;

    public List<Expert> findAll() {

        List<Expert> list = em.createNamedQuery("findAllExpert").getResultList();
        return list;
    }

    public Expert save(Expert expert) {
        em.persist(expert);
        return expert;
    }

    public Expert update(Expert expert) {
        em.merge(expert);
        return expert;
    }

    public Expert delete(Expert expert) {
        em.remove(expert.getId());
        return expert;
    }

    public Expert findById(Long id) {
        Expert expert = em.createNamedQuery("findExpertById", Expert.class).setParameter("id", id).getSingleResult();
        return expert;
    }

}
