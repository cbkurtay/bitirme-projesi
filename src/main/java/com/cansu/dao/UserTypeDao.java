package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.UserType;

@Component
public class UserTypeDao {

    @PersistenceContext
    EntityManager em;

    public List<UserType> findAll() {
        List<UserType> list = em.createNamedQuery("findAllUserType").getResultList();
        return list;
    }

    public UserType save(UserType userType) {
        em.persist(userType);
        return userType;
    }

    public UserType update(UserType userType) {
        em.merge(userType);
        return userType;
    }

    public UserType delete(UserType userType) {
        em.remove(userType.getId());
        return userType;
    }

    public UserType findById(Long id) {
        UserType userType = em.createNamedQuery("findUserTypeById", UserType.class).getSingleResult();
        return userType;
    }

}
