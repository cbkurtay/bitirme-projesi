package com.cansu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cansu.model.Member;

@Component
public class MemberDao {

    @PersistenceContext
    EntityManager em;

    public List<Member> findAll() {
        List<Member> list = em.createNamedQuery("findAllMember").getResultList();
        return list;
    }

    public Member save(Member member) {
        em.persist(member);
        return member;
    }

    public Member update(Member member) {
        em.merge(member);
        return member;
    }

    public Member delete(Member member) {
        em.remove(member.getId());
        return member;
    }

    public Member findById(Long id) {
        Member member = em.createNamedQuery("findMemberById", Member.class).setParameter("id", id).getSingleResult();
        return member;
    }

}
