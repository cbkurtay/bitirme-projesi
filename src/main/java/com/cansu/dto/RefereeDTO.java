package com.cansu.dto;

import com.cansu.model.Referee;

public class RefereeDTO {

    private Long id;

    private String name;

    private String surname;

    private String userName;

    private String mail;

    private String password;

    public Referee toEntity() {
        Referee referee = new Referee();

        referee.setId(this.id);
        referee.setMail(this.mail);
        referee.setName(this.name);
        referee.setPassword(this.password);
        referee.setSurname(this.surname);
        referee.setUserName(this.userName);

        return referee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
