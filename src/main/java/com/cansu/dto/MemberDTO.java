package com.cansu.dto;
import com.cansu.model.Member;

public class MemberDTO {

    private Long id;

    private String name;

    private String surname;

    private String userName;

    private String mail;

    private String experience;

    private String licenceStatus;

    private String password;

    public Member toEntity() {

        Member member = new Member();

        member.setId(this.id);
        member.setMail(this.mail);
        member.setName(this.name);
        member.setLicenceStatus(this.licenceStatus);
        member.setExperience(this.experience);
        member.setPassword(this.password);
        member.setSurname(this.surname);
        member.setUserName(this.userName);

        return member;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLicenceStatus() {
        return licenceStatus;
    }

    public void setLicenceStatus(String licenceStatus) {
        this.licenceStatus = licenceStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
