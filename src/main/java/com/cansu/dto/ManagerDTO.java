package com.cansu.dto;

import com.cansu.model.Manager;

public class ManagerDTO {

    private Long id;

    private String name;

    private String surname;

    private String userName;

    private String password;

    private String mail;

    public Manager toEntity() {
        Manager manager = new Manager();

        manager.setId(this.id);
        manager.setMail(this.mail);
        manager.setName(this.name);
        manager.setPassword(this.password);
        manager.setSurname(this.surname);
        manager.setUserName(this.userName);

        return manager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
