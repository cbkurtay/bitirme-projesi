package com.cansu.dto;

import com.cansu.model.SystemManager;
/**
 * Created by cansubahar on 4.04.2017.
 */
public class SystemManagerDTO {

    private Long id;

    private String name;

    private String surname;

    private String userName;

    private String password;

    private String mail;

    public SystemManager toEntity() {

        SystemManager systemManager = new SystemManager();
        systemManager.setId(this.id);
        systemManager.setMail(this.mail);
        systemManager.setName(this.name);
        systemManager.setPassword(this.password);
        systemManager.setSurname(this.surname);
        systemManager.setUserName(this.userName);

        return systemManager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
