package com.cansu.dto;


import com.cansu.model.UserType;

public class UserTypeDTO {

    private Long id;

    private String name;

    public UserType toEntity() {
        UserType userType = new UserType();

        userType.setId(this.id);
        userType.setName(this.name);
        return userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
