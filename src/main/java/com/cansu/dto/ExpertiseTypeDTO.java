package com.cansu.dto;

import com.cansu.model.ExpertiseType;

public class ExpertiseTypeDTO {

    private Long id;

    private String name;

    public ExpertiseType toEntity() {

        ExpertiseType expertiseType = new ExpertiseType();

        expertiseType.setId(this.id);
        expertiseType.setName(this.name);

        return expertiseType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
