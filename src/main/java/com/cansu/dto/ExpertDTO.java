package com.cansu.dto;


import com.cansu.model.Expert;

public class ExpertDTO {

    private Long id;

    private String name;

    private String surname;

    private String userName;

    private String mail;

    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Expert toEntity() {
        Expert expert = new Expert();
        expert.setId(this.id);
        expert.setMail(this.mail);
        expert.setName(this.name);
        expert.setPassword(this.password);
        expert.setSurname(this.surname);
        expert.setUserName(this.userName);

        return expert;
    }

}
