package com.cansu.dto;

import com.cansu.model.Project;
/**
 * Created by cansubahar on 4.04.2017.
 */
public class ProjectDTO {

    private Long id;

    private String name;

    private String title;

    private String summary;

    private String keywords;

    private String document;

    private String expertEvaluation;

    public Project toEntity() {
        Project project = new Project();

        project.setId(this.id);
        project.setDocument(this.document);
        project.setExpertEvaluation(this.expertEvaluation);
        project.setKeywords(this.keywords);
        project.setName(this.name);
        project.setSummary(this.summary);
        project.setTitle(this.title);

        return project;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getExpertEvaluation() {
        return expertEvaluation;
    }

    public void setExpertEvaluation(String expertEvaluation) {
        this.expertEvaluation = expertEvaluation;
    }
}
