angular.module('takip', ['ui.router'])

.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('test', {
            url: '/proje',
            templateUrl: 'app/templates/test.html',
            controller: 'TestController'
        })

        .state('uyeler',{
            url:'/uyeler',
            templateUrl:'app/templates/kullanicilar.html',
            controller:'KullaniciController'
            }
        )

        .state('uzmanlar',{
            url:'/uzmanlar',
            templateUrl:'app/templates/uzmanlar.html',
            controller:'UzmanlarController'
        })

        .state('hakemler',{
            url:'/hakemler',
            templateUrl:'app/templates/hakemler.html',
            controller:'HakemController'
        })

        .state('yonetici',{
            url:'/yonetici',
            templateUrl:'app/templates/yonetici.html',
            controller:'YoneticiController'
        })

        .state('sistemYoneticisi',{
            url:'/sistemYoneticisi',
            templateUrl:'app/templates/sistemYoneticisi.html',
            controller:'SistemYoneticisiController'
        })

}]);