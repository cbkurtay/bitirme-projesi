angular.module('takip')

    .factory('YoneticiService', ['$http', function ($http) {
        return {
            getManagers: function(){
                return $http.get('/rest/manager');
            }
        }
    }]);