angular.module('takip')

.factory('TestService', ['$http', function ($http) {
    return {
        getProjects: function(){
            return $http.get('/rest/project');
        },

        createProject:function (formData) {
            return $http.post('/rest/project', formData);
        }


    }
}]);