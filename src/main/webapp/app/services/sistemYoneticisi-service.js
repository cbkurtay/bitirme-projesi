angular.module('takip')

    .factory('SistemYoneticisiService', ['$http', function ($http) {
        return {
            getSystemManager: function(){
                return $http.get('/rest/systemManager');
            },

        }

    }]);