angular.module('takip')

    .factory('HakemService', ['$http', function ($http) {
        return {
            getReferees: function(){
                return $http.get('/rest/referee');
            }
        }
    }]);