angular.module('takip')

    .factory('UzmanlarService', ['$http', function ($http) {
        return {
            getExperts: function(){
                return $http.get('/rest/expert');
            },

            deleteExpert: function (expertId) {
                return $http.delete('/rest/expert'+expertId);
            }
        }
    }]);