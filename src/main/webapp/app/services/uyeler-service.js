angular.module('takip')

    .factory('KullaniciService', ['$http', function ($http) {
        return {
            getUsers: function(){
                return $http.get('/rest/member');
            }
        }
    }]);