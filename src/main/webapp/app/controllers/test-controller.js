angular.module('takip')

.controller('TestController', ['$scope', 'TestService','$window', function ($scope, TestService,$window) {
    TestService.getProjects().then(function (response) {
        $scope.projects = response.data;
    }, function () {
        alert("HATA");
    });

    $scope.cancel = function() {
        $scope.project.title="";
        $scope.project.summary="";
        $scope.project.keywords="";
        $scope.project.document="";


    };

    $scope.createProject=function (project) {
        TestService.createProject(project).then(function () {
            "use strict";
            $window.location.reload();
            });
    };
}]);

