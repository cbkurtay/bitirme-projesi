angular.module('takip')

    .controller('UzmanlarController', ['$scope', 'UzmanlarService', function ($scope, UzmanlarService, $window) {
        UzmanlarService.getExperts().then(function (response) {
            $scope.experts = response.data;
        }, function () {
            alert("HATA");
        });

        $scope.deleteExpert=function (expertId){
            "use strict";
            UzmanlarService.deleteExpert({id:expertId})
                .then(function () {
                    $window.location.reload();
                });
        };
    }]);